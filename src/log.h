#ifndef LOG_H
#define LOG_H

#include "pch.h"

namespace lg
{
inline mutex mu;
inline int level = 1;

struct Line
{
        bool op;

        ~Line()
        {
                if (op)
                {
                        clog << endl;
                        mu.unlock();
                }
        }

        Line &operator<<(const auto &x)
        {
                if (op)
                        clog << x;
                return *this;
        }
};

struct Logger
{
        int level;
        const char *name;
        Line operator<<(const auto &x) const
        {
                if (level < lg::level)
                        return {false};
                mu.lock();
                clog << "\033[38;5;8m[\033[0m" << name << now() << "\033[38;5;8m]\033[0m " << x;
                return {true};
        }
};

inline Logger debug(0, "\033[34mD\033[0m ");
inline Logger info(1, "\033[32mI\033[0m ");
inline Logger warn(2, "\033[33mW\033[0m ");
inline Logger error(3, "\033[31mE\033[0m ");

} // namespace lg

#endif
