// trans rights
#include "parse.h"
#include <sys/stat.h>

int n_threads = 16;
bool testing = false;
bool found = false, died = false;
Config config;
int id;

const char *gen, *a, *b;

void parse(int argc, const char *argv[])
{
        vector<bool> used(argc);
        for (int i = 0; i < argc - 1; i++)
        {
                if (strcmp(argv[i], "-g") == 0)
                {
                        used[i] = used[i + 1] = true;
                        gen = argv[i + 1];
                }
                if (strcmp(argv[i], "-t") == 0)
                {
                        used[i] = true;
                        testing = true;
                }
                if (strcmp(argv[i], "-n") == 0)
                {
                        used[i] = used[i + 1] = true;
                        n_threads = stoi(argv[i + 1]);
                }
        }
        for (int i = 1; i < argc; i++)
        {
                if (used[i])
                        continue;
                if (not a)
                        a = argv[i];
                else
                        b = argv[i];
        }
}

void solve()
{
        vector<thread> ts(n_threads);
        int cnt = 0;
        mutex mu;
        for (int i = 0; i < n_threads; i++)
        {
                ts[i] = thread(
                    [&, i]()
                    {
                            while (not found and not died)
                            {
                                    auto fn = format(".dow.d/{}.txt", i);
                                    config.generate(fn.data());
                                    int x, y;
                                    string A = get_output(format("{} < {}", a, fn).data(), &x);
                                    string B = get_output(format("{} < {}", b, fn).data(), &y);
                                    if (x | y)
                                    {
                                            died = true;
                                            break;
                                    }
                                    if (not diff_space(A, B))
                                    {
                                            found = true;
                                            id = i;
                                    }
                                    mu.lock();
                                    cnt++;
                                    mu.unlock();
                            }
                    });
        }
        thread(
            [&]()
            {
                    while (not found and not died)
                    {
                            cerr << "checked " << cnt << " inputs\r";
                            this_thread::sleep_for(chrono::milliseconds(200));
                    }
            })
            .detach();
        for (auto &t : ts)
                t.join();
        cerr << "checked " << cnt << " inputs in total\n";
        if (found)
        {
                cerr << "FOUND @ .dow.d/" << id << ".txt\n";
                char buf[257] = {};
                auto fn = format(".dow.d/{}.txt", id);
                ifstream is(fn);
                is.read(buf, 256);
                cerr << "first " << strlen(buf) << " bytes of input:\n";
                cerr << buf << '\n';

                string A = get_output(format("{} < {}", a, fn).data(), 0);
                string B = get_output(format("{} < {}", b, fn).data(), 0);

                cerr << "output A: " << '\n' << A << '\n';
                cerr << "output B: " << '\n' << B << '\n';
        }
        if (died)
                cerr << "DIED\n";
}

void test()
{
        vector<thread> ts(n_threads);
        vector<string> outputs(n_threads);
        for (int i = 0; i < n_threads; i++)
        {
                ts[i] = thread(
                    [&, i]()
                    {
                            auto fn = format(".dow.d/{}.txt", i);
                            config.generate(fn.data());
                            int x;
                            outputs[i] = get_output(format("{} < {}", a, fn).data(), &x);
                            if (x)
                                    died = true;
                    });
        }
        for (auto &t : ts)
                t.join();
        for (int i = 0; i < n_threads; i++)
        {
                if (i)
                        cerr << string(52, '@') << '\n';
                system(format("cat .dow.d/{}.txt", i).data());
                cerr << string(13, '-') << '\n';
                cerr << outputs[i] << '\n';
        }
        if (died)
                cerr << "DIED\n";
}

int main(int argc, const char *argv[])
{
        ios::sync_with_stdio(0), cin.tie(0);
        if (argc == 1)
        {
                cout << "usage (provided an existing .dow) file:\n\tdow <bin1> <bin2>\n";
                return 0;
        }
        parse(argc, argv);
        mkdir(".dow.d", 0777);
        if (not gen)
                config = parse(".dow");
        else
                config.gen = gen;
        if (testing)
                test();
        else
                solve();
        return 0;
}
