#include "parse.h"
#include "rand.h"

void dispatch(char x, ostream &os) { os << x; }

void dispatch(ll x, ostream &os) { os << x; }

void dispatch(const Range &x, ostream &os) { os << xosh64() % (x.r - x.l + 1) + x.l; }

void dispatch(const Graph &x, ostream &os)
{
        unordered_set<ll> u;
        for (int i = 0; i < x.M;)
        {
                int a = xosh64() % x.N;
                int b = xosh64() % (x.N - 1);
                if (b >= a)
                        b++;
                if (u.find((ll)a * x.N + b) != u.end())
                        continue;
                u.insert((ll)a * x.N + b);
                u.insert((ll)b * x.N + a);
                if (i++)
                        os << '\n';
                os << a + 1 << ' ' << b + 1;
                if (x.weight.has_value())
                {
                        os << ' ';
                        dispatch(*x.weight, os);
                }
        }
}

void dispatch(const String &x, ostream &os)
{
        for (int i = 0; i < x.N; i++)
                os << x.allowed[xosh64() % x.allowed.size()];
}

void Config::generate(const char *fn)
{
        if (gen)
        {
                system(format("{} > {}", gen, fn).data());
                return;
        }
        ofstream os(fn);
        for (auto &c : items)
                visit([&](const auto &x) { dispatch(x, os); }, c);
}

Range Range::parse(char *it, char **p)
{
        Range r;
        r.l = strtoll(++it, &it, 10);
        r.r = strtoll(++it, &it, 10);
        *p = ++it;
        return r;
}

Graph Graph::parse(char *it, char **p)
{
        it++;
        while (isspace(*it))
                it++;
        Graph g;
        g.N = strtoll(it, &it, 10);
        g.M = strtoll(it, &it, 10);
        while (isspace(*it))
                it++;
        if (*it == '[')
        {
                g.weight = Range::parse(it, &it);
                while (isspace(*it))
                        it++;
        }
        else
        {
                g.weight = nullopt;
        }
        *p = ++it;
        return g;
}

String String::parse(char *it, char **p)
{
        it++;
        while (isspace(*it))
                it++;
        String s;
        s.N = strtoll(it, &it, 10);
        while (isspace(*it))
                it++;
        while (*it != ')')
                s.allowed.push_back(*(it++));
        if (s.allowed.empty())
                lg::error << "parse error: |allowed| = 0";
        it++;
        *p = it;
        return s;
}

Config parse(const char *fn)
{
        ifstream is(fn);
        if (not is)
        {
                lg::error << "failed to open " << fn;
                return {};
        }
        string line;
        Config c;
        while (getline(is, line))
        {
                char *it = line.data();
                while (*it)
                {
                        if (isdigit(*it))
                        {
                                ll u = strtoll(it, &it, 10);
                                c.items.push_back(u);
                        }
                        else if (*it == '[')
                        {
                                c.items.push_back(Range::parse(it, &it));
                        }
                        else if (*it == '@')
                        {
                                auto p = ++it;
                                if (strncmp(p, "graph", 5) == 0)
                                {
                                        auto g = Graph::parse(it + 5, &it);
                                        c.items.push_back(g);
                                }
                                else if (strncmp(p, "string", 6) == 0)
                                {
                                        auto s = String::parse(it + 6, &it);
                                        c.items.push_back(s);
                                }
                                else
                                {
                                        lg::error << "parse error @ " << p;
                                }
                        }
                        else
                        {
                                c.items.push_back(*(it++));
                        }
                }
                c.items.push_back('\n');
        }
        return c;
}
