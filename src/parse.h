#ifndef PARSE_H
#define PARSE_H

#include "common.h"

struct Range
{
        ll l, r;
        static Range parse(char *, char **);
};

struct Graph
{
        int N, M;
        optional<Range> weight;
        static Graph parse(char *, char **);
};

struct String
{
        int N;
        string allowed;
        static String parse(char *, char **);
};

using Item = variant<ll, Range, char, Graph, String>;

struct Config
{
        const char *gen = 0;
        vector<Item> items;

        void generate(const char *);
};

Config parse(const char *);

#endif
