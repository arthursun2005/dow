#ifndef RAND_H
#define RAND_H

#include "pch.h"

inline thread_local uint64_t xosh64_state = random_device{}() | 1;

ALWAYS_LINE uint64_t xosh64()
{
        uint64_t x = xosh64_state;
        x ^= x >> 12;
        x ^= x << 25;
        x ^= x >> 27;
        return (xosh64_state = x) * 0x2545F4914F6CDD1DULL;
}

#endif
