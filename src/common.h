#ifndef COMMON_H
#define COMMON_H

#include "log.h"
#include "pch.h"

template <typename... Ts> ostream &operator<<(ostream &os, const variant<Ts...> &v)
{
        visit([&](const auto &x) { os << x; }, v);
        return os;
}

inline string get_output(const char *cmd, int *x = 0)
{
        char buffer[4096];
        string res;
        FILE *fp = popen(cmd, "r");
        if (not fp)
        {
                lg::error << "cannot popen: " << cmd;
                return {};
        }
        while (fgets(buffer, sizeof(buffer), fp))
                res += buffer;
        int u = pclose(fp);
        int code = WEXITSTATUS(u);
        if (code)
                lg::error << "non-zero return code: " << code;
        if (x)
                *x = code;
        return res;
}

inline bool diff_space(const string &A, const string &B)
{
        size_t j = 0;
        for (size_t i = 0; i < A.size(); i++)
        {
                if (isspace(A[i]))
                        continue;
                while (j < B.size() and isspace(B[j]))
                        j++;
                if (j == B.size() or A[i] != B[j])
                        return false;
                j++;
        }
        while (j < B.size() and isspace(B[j]))
                j++;
        return j == B.size();
}

#endif
