#ifndef PCH_H
#define PCH_H

#include <bits/extc++.h>

using namespace std;
using ll = long long;

#define ALWAYS_LINE inline __attribute__((always_inline))

inline auto now() { return chrono::system_clock::now(); }

#endif
